module.exports = {
    optimization: {
        minimize: false,
    },
    target: 'node',
    mode: 'production',
    entry: __dirname + "/app.ts",
    output: {
        path: __dirname + "/distwp",
        filename: "app.js"
    },
    module: {
        rules: [{
            test: /\.tsx?$/,
            use: 'ts-loader',
            exclude: /node_modules/
        }]
    },
    resolve: {
        extensions: ['.ts', '.js', '.json']
    }
};