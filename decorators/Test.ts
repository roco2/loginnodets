import { Request, Response } from "express";

export function routeLog(role: string): MethodDecorator  {
  return function (
    target: Object,
    propertyKey: string | symbol,
    descriptor: PropertyDescriptor
) {
    const original = descriptor.value;
    
    // tslint:disable-next-line:no-any
    descriptor.value = function (...args: any[]) {
        const request = args[0] as Request;
        const response = args[1] as Response;
        
        const {
            url,
            method,
            body,
            headers,
        } = request;

        if (!(body.role === role)) {
          return response.status(403).json({error: "Not Authorized"});
        }

        console.log("[LOG]", {
            url,
            method,
            body,
            headers,
            original
        });
        return original.apply(this, args);
    }
};
}