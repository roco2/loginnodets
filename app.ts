import {
  ConnectionOptions,
  createConnection,
  getConnectionOptions,
} from "typeorm";
import express from "express";
import cors from "cors";
import helmet from "helmet";
import * as dotenv from "dotenv";

import routes from "./src/routes";
import { User } from "./src/entity/User";
import corsOptionsDelegate from "./src/middleware/validatecors";
import debug from "debug";

const start = async () => {
  dotenv.config();
  const baseConnectionOpts = await getConnectionOptions();
  await init(baseConnectionOpts);
};

const init = (baseConnectionOpts: ConnectionOptions) => {
  createConnection({
    ...baseConnectionOpts,
    entities: [User],
  })
    .then(async (connection) => {
      // create express app
      const PORT = process.env.PORT || 3000;
      const app = express();
      app.use(cors(corsOptionsDelegate));
      app.use(helmet());
      app.use(express.json());
      app.use("/", routes);
      // const debugLog: debug.IDebugger = debug("app");
      app.listen(PORT, () => {
        console.log(`Server running at http://localhost:${PORT}`);
        // debugLog(`Server running at http://localhost:${PORT}`);
      });
      
    })
    .catch((error) => console.log(error));
};

start();
