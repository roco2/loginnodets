import {getRepository} from "typeorm";
import {Request, Response} from "express";
import {User} from "../entity/User";
import * as jwt from "jsonwebtoken";
import { routeLog } from "../../decorators/Test";

class AuthController {
    
    // @routeLog("Admin")
    public static async login(req: Request, res: Response) {
        const {username, password} = req.body;

        if (!(username && password)) {
            return res.status(400).json({message:"Username & Password are required!"});
        }

        const userRepository = getRepository(User);
        let user: User;

        try {
            user = await userRepository.findOneOrFail({where:{username}});
        } catch (error) {
            return res.status(400).json({message: "User or password incorrect!"});
        }

        //Check Pass

        if (!user.checkPassword(password)) {
            return res.status(400).json({message:" Username or pass incorrect!"})
        }
        const token = jwt.sign(
            {
              userId: user.id,
              username: user.username,
              role: user.role
            },
            "SECRET",
            {
              expiresIn: "1h",
              algorithm: "HS512"
            }
          );
        return res.status(200).json({  message: "Logged", status: true, token });
    }

}

export default AuthController;

