import { Request, Response, NextFunction } from "express";
import * as jwt from "jsonwebtoken";

export const validateJwt = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const token = <string>req.headers["auth"];
  let jwtPayload;
  try {
    // tslint:disable-next-line:no-any
    jwtPayload = <any>jwt.verify(token, "SECRET");
    res.locals.jwtPayload = jwtPayload;
  } catch (e) {
    return res.status(401).json({message: "Not Authorized"});
  }
  const { userId, username } = jwtPayload;
  const newTkn = jwt.sign({ userId, username }, "SECRET", { expiresIn: "1h" });
  res.setHeader("token", newTkn);

  next();
};
