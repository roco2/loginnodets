import { CorsOptions } from "cors";
import { Request } from "express";

const corsOptionsDelegate = async (
  req: Request,
  // tslint:disable-next-line:no-any
  callback: any
// tslint:disable-next-line:no-any
): Promise<any> => {
  const myIpAddress = req.ip || "";
  // tslint:disable-next-line:no-any
  let msg: any = null;
  const METHODLIST = process.env.METHODLIST || "";
  const ORIGINLIST = process.env.ORIGINLIST || "";
  const originList = ORIGINLIST.split(",");
  const methodsarray = METHODLIST.split(",");
  const corsOptions: CorsOptions = {
    methods: methodsarray,
    allowedHeaders: ["Content-Type", "Authorization"],
    credentials: true,
  };

  if (
    originList.indexOf(myIpAddress) !== -1 &&
    methodsarray.indexOf(req.method) !== -1
  ) { 
    corsOptions.origin = true;
  }
   else {
    corsOptions.origin = false;
    msg =
      "The CORS policy for this site does not allow access from the specified Origin";
  }

  return callback(msg, corsOptions.origin);
};

export default corsOptionsDelegate;
