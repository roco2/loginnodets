import {Router} from "express";
import UserController from "../controller/UserController";
import { validateJwt } from "../middleware/jwt";

const router = Router();

//Get all users
router.get("/", [validateJwt], UserController.getAll);

//Get one user
router.get("/:id", [validateJwt], UserController.getById);

// Create new user
router.post("/", [validateJwt], UserController.newUser);

// Edit new user
router.patch("/:id", [validateJwt], UserController.editUser);

// Delete new user
router.delete("/:id", [validateJwt], UserController.deleteUser);

export default router;